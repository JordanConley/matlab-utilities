function [ val ] = ReciprocalAdd(x)

val = 0;

for i = 1:numel(x) 
    val = val + 1/x(i);
end

val = 1 / val;
end

