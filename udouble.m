classdef udouble
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        x
        dx
    end
    
    methods        
        function obj = udouble(val)
            obj.x = val(1);
            obj.dx = val(2);
        end
        
        function c = double(obj)
            c = obj.x;
        end
        
        function r = plus(a,b)
            if isa(a, 'double')
                a = udouble([a 0]);
            end
            
            if isa(b, 'double')
                b = udouble([b 0]);
            end
            r = udouble([0 0]);
            r.x = a.x + b.x;
            r.dx = sqrt((a.dx)^2 + (b.dx)^2);
        end
        
        function r = minus(a,b)
            if isa(a, 'double')
                a = udouble([a 0]);
            end
            
            if isa(b, 'double')
                b = udouble([b 0]);
            end
            r = udouble([0 0]);
            r.x = a.x - b.x;
            r.dx = sqrt((a.dx)^2 + (b.dx)^2);
        end
        
        function r = times(a,b)
            if isa(a, 'double')
                a = udouble([a 0]);
            end
            
            if isa(b, 'double')
                b = udouble([b 0]);
            end
            r = udouble([0 0]);
            r.x = a.x * b.x;
            r.dx = abs(r.x * sqrt((a.dx/a.x)^2 + (b.dx/b.x)^2));
        end
        
        function r = mtimes(a, b)
            r = times(a,b);
        end
        
        function r = rdivide(a,b)
            if isa(a, 'double')
                a = udouble([a 0]);
            end
            
            if isa(b, 'double')
                b = udouble([b 0]);
            end
            r = udouble([0 0]);
            r.x = a.x / b.x;
            r.dx = abs(r.x * sqrt((a.dx/a.x)^2 + (b.dx/b.x)^2));
        end
        
        function r = mrdivide(a,b)
            r = rdivide(a,b);
        end
        
        function r = mpower(a, b)
            r = udouble([a.x^b 0]);
            
            r.dx = r.x * b * (a.dx/a.x);
        end
        
        function r = power(a, b)
            r = mpower(a,b);
        end
        
        function r = sqrt(a)
            r = mpower(a,1/2);
        end
        
        function r = log10(a)
            r = udouble([log10(a.x) a.dx/(2.3*a.x)]);
        end
        
        function r = sin(a)
            r = udouble([sin(a.x) abs(cos(a.x)*a.dx)]);
        end
        
        function r = cos(a)
            r = sin(a + pi/2);
        end
    end
    
end